import tornado.ioloop
import tornado.web
from tornado.options import define, options,parse_command_line
import json
import random


#define("port", default=4444, help="run on the given port", type=int)


class Dashboard(tornado.web.RequestHandler):
    def get(self):
	self.render("id.htm")

class Dashboard1(tornado.web.RequestHandler):
    def get(self, lab_id):
        self.render("id1.htm")

class WebHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("This Page is down for maintenance")

class LabHandler(tornado.web.RequestHandler):
    def get(self):
        obj = [
		    {"lab_id": "cse02", "status": "running"},
		    {"lab_id": "ece34", "status": "stopped"},
		    {"lab_id": "civ04", "status": "running"},
                    {"lab_id": "eee45", "status": "stopped"},
                    {"lab_id": "mec23", "status": "running"},
                    {"lab_id": "cce01", "status": "stopped"},
                    {"lab_id": "mit03", "status": "stopped"}
	      ]
        self.write(json.dumps(obj))  
       

class Labinfo(tornado.web.RequestHandler):
    def get(self, lab_id):
        response = {
		     "lab_id": lab_id ,
                     "resources":{ 
                                   "ram_usage":random.random(),
                                   "disk_usage":random.random(),
                                   "cpu_units":random.randint(10,20),
                                   "up_time":random.randint(0,30),
                     },
                     "infrastructure":{
                                      "data_centre":"iiit",
                                      "os":"ubuntu"
                     },
                     "usage_analytics":{
                                        "hits_per_day":random.randint(20,100),
                                        "unique_hits_per_day":random.randint(0,30)
                     }
                   }
                           
        self.write(json.dumps(response))

application = tornado.web.Application([
    (r"/", WebHandler),
    (r"/labs", LabHandler),
    (r"/labs/([a-z]{3}[0-9]+)", Labinfo),
    (r"/dashboard", Dashboard),
    (r"/dashboard/([a-z]{3}[0-9]+)", Dashboard1)
], debug = True)

if __name__ == "__main__":
    application.listen(9889)
    tornado.ioloop.IOLoop.instance().start()
